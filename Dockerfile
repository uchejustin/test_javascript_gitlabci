FROM node:alpine


WORKDIR '/app'

COPY package.json yarn.lock ./

RUN yarn install --production=false

COPY . .

RUN yarn lint

CMD ["npm", "run", "start"]